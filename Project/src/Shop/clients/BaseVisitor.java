package Shop.clients;

import Shop.interfaces.IGood;
import Shop.interfaces.IVisitor;

public abstract class BaseVisitor implements IVisitor, IGood {

    public String name;

    @Override
    public String getName(){
        return name;
    }

    @Override
    public void buy(IGood good) {
        System.out.println(good);

    }

    @Override
    public void returnGoods() {

    }

    BaseVisitor(){

    }

}
