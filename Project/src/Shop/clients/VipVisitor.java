package Shop.clients;

import Shop.interfaces.IGood;

public class VipVisitor extends BaseVisitor{

    private int discount;

      public boolean checkDiscount() {
        if (discount > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void buy(IGood good) {
        if (checkDiscount()) {

        } else {
            super.buy(good);
        }

    }

}
