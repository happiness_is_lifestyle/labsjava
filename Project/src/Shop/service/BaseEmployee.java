package Shop.service;

import Shop.departments.BaseDepartment;
import Shop.interfaces.IEmployee;

public abstract class BaseEmployee implements IEmployee {

    public String name;
    BaseDepartment department;
    public String employment;
    public boolean free;

    @Override
    public String getEmployment(){
        return employment;
    }

    @Override
    public String getName(){
        return name;
    }

    @Override
    public boolean isFree() {

        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    BaseEmployee(){}
}
