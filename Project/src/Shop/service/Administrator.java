package Shop.service;

import java.util.ArrayList;
import Shop.interfaces.IDepartment;
import Shop.interfaces.IEmployee;


public class Administrator {
    private String name;
    private boolean free;
    ArrayList<Administrator>salesRoom;


    public Consultant getFreeConsultant(IDepartment iDepartment) {
        for (IEmployee employee : iDepartment.getEmployEesList()) {
            if (employee instanceof Consultant) {

                if (employee.isFree()) {
                    return (Consultant)employee;
                }
            }
        }
        return null;
    }


    public boolean isFree() {
        return free;
    }
}
