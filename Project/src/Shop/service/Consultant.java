package Shop.service;

import Shop.interfaces.IVisitor;
import Shop.enums.ConsultResult;

public class Consultant extends BaseEmployee {

    public ConsultResult consult(IVisitor visitor) {
        super.setFree(false);
        return ConsultResult.BUY;
    }

    public void send() {
    }

}