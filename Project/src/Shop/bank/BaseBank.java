package Shop.bank;

import Shop.interfaces.IBank;

public abstract class BaseBank implements IBank {
    public String name;
    public String creditDescription;

    @Override
    public String getCreditDescription(){
        return creditDescription;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void checkInfo() {

    }
    @Override
    public void giveCredit() {

    }

}
