package Shop.interfaces;

public interface IVisitor {

    String getName();

    void buy(IGood good);

    void returnGoods();

}
