package Shop.interfaces;

import Shop.service.Consultant;

import java.util.ArrayList;

import Shop.interfaces.IEmployee;
import Shop.departments.BaseDepartment;
import Shop.service.BaseEmployee;

public interface IDepartment {
    String getName();

    void buy();

    void returnGoods();

    ArrayList<IEmployee> getEmployEesList();




}
