package Shop.interfaces;

public interface IEmployee {
    String getName();
    String getEmployment();
    boolean isFree();
}
