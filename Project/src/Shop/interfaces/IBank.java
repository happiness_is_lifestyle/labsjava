package Shop.interfaces;

public interface IBank {

    String getCreditDescription();
    String getName();

    void checkInfo();

    void giveCredit();
}
