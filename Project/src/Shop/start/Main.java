package Shop.start;


import Shop.SalesRoom;
import Shop.clients.Visitor;
import Shop.departments.ElectronicDepartment;
import Shop.departments.GameDepartment;
import Shop.goods.ElectronicDevice;
import Shop.goods.GameConsole;
import Shop.service.Administrator;
import Shop.enums.ConsultResult;
import Shop.service.Consultant;

public class Main {
    public static void main(String[] args){
     imitateShopWorking();
    }
    static final String SHOP_NAME = "Магазин";
    public static void imitateShopWorking(){
        SalesRoom salesroom = new  SalesRoom(SHOP_NAME);
        Administrator administrator = new Administrator();
        ElectronicDepartment electronicdepartment = new ElectronicDepartment();
        electronicdepartment.addemployee();
        electronicdepartment.getBanker().isFree();
        electronicdepartment.getCashier().isFree();
        electronicdepartment.getConsultant().isFree();
        electronicdepartment.getSecurity().isFree();
        ElectronicDevice electronicDevice1 = new ElectronicDevice();
        electronicdepartment.addElectronicDevice(electronicDevice1);



        GameDepartment gamedepartment = new GameDepartment();
        gamedepartment.addemployee();
        gamedepartment.getBanker().isFree();
        gamedepartment.getCashier().isFree();
        gamedepartment.getConsultant().isFree();
        gamedepartment.getSecurity().isFree();
        GameConsole gameConsole1 = new GameConsole();
        gamedepartment.addGameConsole(gameConsole1);

        Visitor visitor1 = new Visitor();
        Visitor visitor2 = new Visitor();


        Consultant consultant1 = administrator.getFreeConsultant(electronicdepartment);
        Consultant consultant2 = administrator.getFreeConsultant(gamedepartment);

        ConsultResult consultResult1 = consultant1.consult(visitor1);
        ConsultResult consultResult2 = consultant2.consult(visitor2);


        switch (consultResult1) {
            case BUY:
                visitor1.buy(electronicDevice1);
                break;
            case EXIT:
                break;
        }

        switch (consultResult2) {
            case BUY:
                visitor2.buy(gameConsole1);
                break;
            case EXIT:
                break;
        }





    }
}
