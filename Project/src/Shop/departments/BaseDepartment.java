package Shop.departments;

import Shop.interfaces.IDepartment;
import Shop.interfaces.IEmployee;
import Shop.service.Banker;
import Shop.service.Cashier;
import Shop.service.Consultant;
import Shop.service.Security;

import java.util.ArrayList;

public abstract class BaseDepartment implements IDepartment {
    public String name;
    ArrayList<IDepartment>goods,employees;
    ArrayList <IEmployee> employEesList = new ArrayList<>();
    Banker banker;
    Cashier cashier;
    Consultant consultant;
    Security security;


    @Override
    public String getName(){
        return name;
    }

    @Override
    public void buy() {

    }

    @Override
    public void returnGoods() {

    }

    BaseDepartment(){

    }


    public void addemployee(){
        banker = new Banker();
        cashier = new Cashier();
        consultant = new Consultant();
        security = new Security();
    }

    public Banker getBanker() {
        return banker;
    }
    public Cashier getCashier(){
        return cashier;
    }

    public Consultant getConsultant() {
        return consultant;
    }

    public Security getSecurity() {
        return security;
    }

    public ArrayList<IEmployee> getEmployEesList() { return employEesList; }


}

