package Shop.departments;

import Shop.goods.ElectronicDevice;
import Shop.interfaces.IElectronicDevice;

public class ElectronicDepartment extends BaseDepartment implements IElectronicDevice {


    public ElectronicDevice electronicDevice;
    public void addElectronicDevice(ElectronicDevice electronicDeviceValue) {
        electronicDevice = electronicDeviceValue;

    }


    public boolean hasGuarantee;
    @Override
    public boolean isHasGuaratee() {
        return hasGuarantee;
    }

    public ElectronicDepartment(){

    }
}
